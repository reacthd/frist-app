import React, { Component } from 'react';
import { View, Text } from 'react-native';
import Header from './Header';

export default class Home extends Component {
    render() {
        return (
            <View style={styles.container}>
                <Header
                    title={'Home'}/>
            </View>
        )
    }
};

const styles = {
    container: {
        justifyContent: 'center',
        alignItems: 'center'
    }
}