import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity, Dimensions } from 'react-native';
const screen=Dimensions.get('window');

export default class Header extends Component {
    render() {
        return (
            <View style={styles.container}>
                <TouchableOpacity onPress={this.props.onPressLeft}>
                    <Image source={(this.props.LeftButton) == undefined ? null : this.props.LeftButton}
                        style={(this.props.styleLeftButton) == undefined ? styles.leftIcon : this.props.styleLeftButton} 
                        />
                </TouchableOpacity>
                <Text style={(this.props.styleText) == undefined ? styles.text : this.props.styleText}>
                    {this.props.title}
                </Text>
                <TouchableOpacity onPress={this.props.onPressRight}>
                    <Image source={(this.props.RightButton) == undefined ? null : this.props.RightButton}
                        style={{ height: 25, width: 25 }} />
                </TouchableOpacity>
            </View>
        );
    }
}
 
export const styles = {
    container: {
        width: screen.width,
        height: 80,
        backgroundColor: '#4267b2',
        justifyContent: 'space-between',
        alignItems: 'flex-end',
        flexDirection: 'row',
        padding: 10
    },
    text: {
        color: '#fff',
        fontSize: 18,
        padding: 3
    },
    leftIcon: {
        height: 25,
        width: 25
    }
};
