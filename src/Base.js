import React, { Component } from 'react';
import { View, Text } from 'react-native';

export default class Base extends Component {
    render() {
        return (
            <View style={styles.container}>
                <Text>this is base </Text>
            </View>
        )
    }
};

const styles = {
    container: {
        justifyContent: 'center',
        alignItems: 'center'
    }
}